package me.bynetherdude.easychess;

import me.bynetherdude.easychess.commands.ChessCommand;
import me.bynetherdude.easychess.events.OnInteract;
import me.bynetherdude.easychess.game.Game;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public final class EasyChess extends JavaPlugin {

    private static final String prefix = "§f[§8Chess§f]§7 ";
    private static HashMap<Player, Game> runningMatches = new HashMap<>();

    @Override
    public void onEnable() {
        System.out.println(getPrefix() + "Chess successfully enabled!");
        commandRegistration();
        eventRegistration();
    }

    private void commandRegistration() {
        getCommand("chess").setExecutor(new ChessCommand());
    }

    private void eventRegistration() {
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new OnInteract(), this);
    }

    public static String getPrefix() {
        return prefix;
    }

    public static void addMatch(Player creator, Game game) {
        runningMatches.putIfAbsent(creator, game);
    }

    public static void removeMatch(Player creator, Game game) {
        runningMatches.remove(creator, game);
    }

    public static HashMap<Player, Game> getRunningMatches() {
        return runningMatches;
    }
}
