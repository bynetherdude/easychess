package me.bynetherdude.easychess.commands;

import me.bynetherdude.easychess.EasyChess;
import me.bynetherdude.easychess.game.Game;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ChessCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!(sender instanceof Player)) {
            System.out.println(EasyChess.getPrefix() + "This command can only be used by Players!");
            return true;
        }
        Player p = (Player) sender;
        if(args.length != 1) {
            printUsage(p);
            return true;
        }

        Player opponent = Bukkit.getPlayer(args[0]);

        if(opponent == null) {
            p.sendMessage(EasyChess.getPrefix() + "§cPlayer not found!");
            return true;
        }
        if(opponent.getUniqueId() == p.getUniqueId()) {
            p.sendMessage(EasyChess.getPrefix() + "§cYou can't play against yourself!");
            return true;
        }

        Game game = new Game(p, opponent);
        game.start();
        return true;
    }

    private void printUsage(Player p) {
        p.sendMessage(EasyChess.getPrefix() + "§cUsage: /chess <opponent>");
    }
}
