package me.bynetherdude.easychess.events;

import me.bynetherdude.easychess.EasyChess;
import me.bynetherdude.easychess.game.Game;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;

public class OnInteract implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractAtEntityEvent e) {
        if(e.getRightClicked() instanceof ArmorStand) {
            e.setCancelled(true); // Cancel it so nobody can remove the skull

            Player p = e.getPlayer();
            p.sendMessage(EasyChess.getPrefix() + "Calculating possible fields..");
            p.sendMessage(e.getRightClicked().getName());

            String[] positions = e.getRightClicked().getName().split(",");
            if(EasyChess.getRunningMatches().containsKey(p)) {
                Game game = EasyChess.getRunningMatches().get(p);
                game.calculateNextMove(Integer.parseInt(positions[0]), Integer.parseInt(positions[1]));
                return;
            }
            p.sendMessage(EasyChess.getPrefix() + "§cYou are not currently in a game!");
        }
    }
}
