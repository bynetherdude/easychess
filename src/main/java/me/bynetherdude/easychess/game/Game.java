package me.bynetherdude.easychess.game;

import me.bynetherdude.easychess.EasyChess;
import me.bynetherdude.easychess.enums.Color;
import me.bynetherdude.easychess.pieces.Piece;
import me.bynetherdude.easychess.setup.Chessboard;
import me.bynetherdude.easychess.utils.MoveCalculator;
import org.bukkit.entity.Player;

public class Game {

    private Chessboard chessboard;
    private final Player creator;
    private final Player opponent;

    public Game(Player creator, Player opponent) {
        this.creator = creator;
        this.opponent = opponent;
    }

    public void start() {
        EasyChess.addMatch(creator, this);
        creator.sendMessage(EasyChess.getPrefix() + "§aGame against §6" + opponent.getName() + "§a started!");
        opponent.sendMessage(EasyChess.getPrefix() + "§6" + creator.getName() + "§a started a game against you!");
        chessboard = new Chessboard(creator);
    }

    public void calculateNextMove(int row, int col) {
        MoveCalculator move = new MoveCalculator(chessboard.getBoard(), row, col);
        move.checkPossibleMoves();
    }
}
