package me.bynetherdude.easychess.pieces;

import me.bynetherdude.easychess.enums.Color;

public class Bishop extends Piece {
    private final String name = "Bishop";
    private Color color;

    public Bishop(Color color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }
}
