package me.bynetherdude.easychess.pieces;

import me.bynetherdude.easychess.enums.Color;

public class King extends Piece {
    private final String name = "King";
    private Color color;

    public King(Color color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }
}
