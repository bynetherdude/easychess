package me.bynetherdude.easychess.pieces;

import me.bynetherdude.easychess.enums.Color;

public class Knight extends Piece {
    private String name = "Knight";
    private Color color;

    public Knight(Color color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }
}
