package me.bynetherdude.easychess.pieces;

import me.bynetherdude.easychess.enums.Color;

public class Pawn extends Piece {
    private final String name = "Pawn";
    private Color color;

    public Pawn(Color color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }
}
