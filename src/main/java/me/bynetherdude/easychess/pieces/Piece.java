package me.bynetherdude.easychess.pieces;

import me.bynetherdude.easychess.enums.Color;

public class Piece {
    private String name = "Test";
    private Color color;

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }
}
