package me.bynetherdude.easychess.pieces;

import me.bynetherdude.easychess.enums.Color;

public class Queen extends Piece {
    private final String name = "Queen";
    private Color color;

    public Queen(Color color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }
}
