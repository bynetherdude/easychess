package me.bynetherdude.easychess.pieces;

import me.bynetherdude.easychess.enums.Color;

public class Rook extends Piece {
    private final String name = "Rook";
    private Color color;

    public Rook(Color color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }
}
