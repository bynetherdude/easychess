package me.bynetherdude.easychess.setup;

import dev.dbassett.skullcreator.SkullCreator;
import me.bynetherdude.easychess.enums.Color;
import me.bynetherdude.easychess.pieces.*;
import me.bynetherdude.easychess.utils.GeneralUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Objects;

public class Chessboard {
    private Field[][] board;
    Player creator;

    public Chessboard(Player creator) {
        this.creator = creator;
        board = new Field[8][8];
        build();
    }

    public Field[][] getBoard() {
        return this.board;
    }

    public void build() {
        summonBlocks();
        summonPieces();
    }

    /**
     * Summons all blocks and initializes fields in board array
     */
    private void summonBlocks() {
        Location location = creator.getLocation();
        Material block = Material.BLACK_CONCRETE;
        Color color;
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                // Set material and color
                block = GeneralUtils.getBlockFromPos(row, col);
                color = GeneralUtils.getColorFromPos(row);

                // Generate at location
                Location tempLoc = new Location(location.getWorld(), location.getX(), location.getY() - 1, location.getZ()); // Clone location, Y-1
                tempLoc.setX(tempLoc.getX() + row);
                tempLoc.setZ(tempLoc.getZ() + col);
                Block tempBlock = tempLoc.getBlock();
                tempBlock.setType(block);

                // Set board field
                board[row][col] = new Field(null, color, tempLoc);
            }
        }
    }

    /**
     * Summons all heads (pieces) and adds the piece objects to the board array
     */
    private void summonPieces() {
        Location location = creator.getLocation();
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                // Exit early if field as no piece
                if (!GeneralUtils.fieldHasPiece(row)) {
                    continue;
                }

                // Generate at location
                Location tempLoc;
                if (GeneralUtils.isPawnRow(row)) {
                    tempLoc = new Location(location.getWorld(), location.getX(), ((int) location.getY()) - 1.5, location.getZ()); // Clone location, Y-1.5
                } else {
                    tempLoc = new Location(location.getWorld(), location.getX(), (int) location.getY() - 1, location.getZ()); // Clone location, Y-1
                }

                // Align position in the middle of the block
                tempLoc.setX(((int) tempLoc.getX() + row) - 0.5);
                tempLoc.setZ(((int) tempLoc.getZ() + col) - 0.5);

                // Generate armorstand
                ArmorStand armorStand = Objects.requireNonNull(location.getWorld()).spawn(tempLoc, ArmorStand.class);
                armorStand.setGravity(false);
                armorStand.setCanPickupItems(false);
                armorStand.setVisible(false);
                armorStand.setCustomName(col + 1 + "," + (row + 1)); // Give position from 1-8 | 1-8 - i=row, j=col

                // Set head
                ItemStack head = SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/" + GeneralUtils.getPieceUrlForPosition(row, col));
                armorStand.getEquipment().setHelmet(head);

                // Set piece in board array
                setPieceInBoard(row, col);
            }
        }
    }

    private void setPieceInBoard(int row, int col) {
        // Pawns
        if(row == 0 || row == 7) {
            board[row][col].setPiece(new Pawn(GeneralUtils.getColorFromPos(row)));
            return;
        }

        // Other pieces
        if(col == 0 || col == 7) {
            board[row][col].setPiece(new Rook(GeneralUtils.getColorFromPos(row)));
            return;
        }
        if(col == 1 || col == 6) {
            board[row][col].setPiece(new Knight(GeneralUtils.getColorFromPos(row)));
            return;
        }
        if(col == 2 || col == 5) {
            board[row][col].setPiece(new Bishop(GeneralUtils.getColorFromPos(row)));
            return;
        }
        if(col == 3) {
            board[row][col].setPiece(new Queen(GeneralUtils.getColorFromPos(row)));
            return;
        }
        if(col == 4) {
            board[row][col].setPiece(new King(GeneralUtils.getColorFromPos(row)));
            return;
        }
    }
}
