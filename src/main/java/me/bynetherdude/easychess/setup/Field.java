package me.bynetherdude.easychess.setup;

import me.bynetherdude.easychess.enums.Color;
import me.bynetherdude.easychess.pieces.Piece;
import org.bukkit.Location;

public class Field {
    private final Color color;
    private Piece piece;
    private Location location;

    public Field(Piece piece, Color color, Location location) {
        this.piece = piece;
        this.color = color;
        this.location = location;
    }

    public Color getColor() {
        return color;
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }
}
