package me.bynetherdude.easychess.utils;

import me.bynetherdude.easychess.enums.Color;
import org.bukkit.Bukkit;
import org.bukkit.Material;

public class GeneralUtils {
    public static String getPieceUrlForPosition(int i, int j) {
        // Increase variables for easier understanding of positions on the board
        i++;
        j++;
        if(i == 7) {
            return "4b7aa4969ae3df8334958c212050be343854303767c29d1a1d11c4ac7b7b53d4"; // Exit with black pawn
        }
        if(i == 2) {
            return "c108c58582bcf2166cd2c0858c73b0af5d1b081f9d628c1b076972c6eb6f0e30"; // Exit with white pawn
        }
        if(i == 8 && (j == 1 || j == 8)) {
            return "7ca26c1c0e19b736ba22f7b31157af920fed61830bd20471b4a886ec971c6323"; // Black rook
        }
        if(i == 8 && (j == 2 || j == 7)) {
            return "5a550c4f2626f47a9270bd82ee8ce70189ff2941900ac8934dcfe88f2d2181d2"; // Black knight
        }
        if(i == 8 && (j == 3 || j == 6)) {
            return "4ddacb1907d3ef3f304210b4f9ab75bd4fc1c9f21b25350904d00cd949f19388"; // Black bishop
        }
        if(i == 8 && j == 4) {
            return "209b3d23a7af275a56402123428186e52965ca9373336ddc112792b415aba0a0"; // Black king
        }
        if(i == 8 && j == 5) {
            return "c33b3ce86d571690b19f5d6d7aa0a721eec70a77962730c771a0815d71503899"; // Black queen
        }
        if(i == 1 && (j == 1 || j == 8)) {
            return "2817755e0a13d5460d183130a5fcdd2e5c38e7292bcd6ef5fdc990501812ac39"; // White rook
        }
        if(i == 1 && (j == 2 || j == 7)) {
            return "8c5decd64c2324945e9214eed2297eddb626c436543fbd413930f12874f2b801"; // White knight
        }
        if(i == 1 && (j == 3 || j == 6)) {
            return "cf9dc78ddc4d732f5e3ed634f74b3ed0b811ed6d52f38c13f2f6db3e99ac084f"; // White bishop
        }
        if(i == 1 && j == 4) {
            return "7ec2822c66ea3a523a8b3c6820580c8c44fdf373b5dc3f55f70028d6cf6d2e44"; // White king
        }
        if(i == 1 && j == 5) {
            return "a62cfaf38deaee3d08d965ce6dc580c8ccd75d3b14d1bfeb4e093726b6c7b1e2"; // White queen
        }
        Bukkit.broadcastMessage("Null");
        return null;
    }

    /**
     * Starts at 0 because of array management
     */
    public static boolean fieldHasPiece(int i) {
        return (i == 0 || i == 1 || i == 6 || i == 7);
    }

    /**
     * Starts at 0 because of array management
     */
    public static boolean isPawnRow(int i) {
        return (i == 1 || i == 6);
    }

    public static Color getColorFromPos(int row) {
        if(row == 0 || row == 1) {
           return Color.WHITE;
        }
        return Color.BLACK;
    }

    public static Material getBlockFromPos(int row, int col) {
        if(row % 2 != 0 && col % 2 != 0) {
            return Material.BLACK_CONCRETE;
        }
        else if(row % 2 == 0 && col % 2 == 0) {
            return Material.BLACK_CONCRETE;
        }
        return Material.WHITE_CONCRETE;
    }
}
