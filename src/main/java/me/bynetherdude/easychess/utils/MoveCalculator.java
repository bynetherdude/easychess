package me.bynetherdude.easychess.utils;

import me.bynetherdude.easychess.pieces.Piece;
import me.bynetherdude.easychess.setup.Field;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;

public class MoveCalculator {
    private final Field[][] board;
    private final int posRow;
    private final int posCol;
    private List<Field> positions;

    public MoveCalculator(Field[][] board, int posRow, int posCol) {
        this.board = board;
        this.posRow = posRow -1; // Prepare for array management
        this.posCol = posCol -1; // Prepare for array management
        this.positions =new ArrayList<Field>();
    }

    public int[][] checkPossibleMoves() {
        Bukkit.broadcastMessage("Column: " + posCol);
        Bukkit.broadcastMessage("Row: " + posRow);
        Field field = board[posRow][posCol];
        Bukkit.broadcastMessage(field.getPiece().getName());
        //Piece piece = board[posCol][posRow];
        //Bukkit.broadcastMessage(piece.getName());
        return checkForPawn();
    }

    private int[][] checkForPawn() {
        if(board[posRow + 1][posCol] == null) {
            //positions.add()
        }
        return new int[][]{};
    }
}
